# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(email: 'ivan@hpup.co', password: 'test')
carrier = Carrier.create(name: 'TestCarrier', user: user)

departure = Departure.create(
  source: 'Rijeka',
  destination: 'Zagreb',
  departure: DateTime.civil_from_format(:utc, 2018, 5, 25, 12, 00),
  arrival: DateTime.civil_from_format(:utc, 2018, 5, 25, 14, 30),
  capacity: 40,
  price: 89.95,
  carrier: carrier
)