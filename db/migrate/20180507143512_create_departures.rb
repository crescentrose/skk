class CreateDepartures < ActiveRecord::Migration[5.2]
  def change
    create_table :departures do |t|
      t.string :source
      t.string :destination
      t.datetime :departure
      t.datetime :arrival
      t.integer :capacity
      t.float :price
      t.references :carrier, foreign_key: true

      t.timestamps
    end
  end
end
