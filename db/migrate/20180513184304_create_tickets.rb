class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :code
      t.references :departure, foreign_key: true
      t.references :user, foreign_key: true
      t.references :transaction, foreign_key: true

      t.timestamps
    end
  end
end
