class DeserializableUser < JSONAPI::Deserializable::Resource
  attributes :email, :password
end