class SerializableTransaction < JSONAPI::Serializable::Resource
  type 'transactions'

  attributes :credit_card, :sum

  belongs_to :user
  belongs_to :departure

  has_one :ticket
end