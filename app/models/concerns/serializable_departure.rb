class SerializableDeparture < JSONAPI::Serializable::Resource
  type 'departures'

  attributes :source, :destination, :price

  attribute :departure do
    format_date(@object.departure)
  end

  attribute :arrival do
    format_date(@object.arrival)
  end

  attribute :availableSeats do
    @object.available_seats
  end

  belongs_to :carrier

  private

  def format_date(date)
    date.strftime('%F %T')
  end
end