class DeserializableTransaction < JSONAPI::Deserializable::Resource
  attribute :creditCard do |cc|
    { credit_card: cc }
  end

  has_one :departure
end