class DeserializableDeparture < JSONAPI::Deserializable::Resource
  attributes :source, :destination, :price, :departure, :arrival, :capacity

  has_one :carrier

end