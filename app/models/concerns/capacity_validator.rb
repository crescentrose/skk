class CapacityValidator < ActiveModel::Validator
  def validate(record)
    unless record.departure.capacity > 0
      record.errors[:departure] << 'This line is already full.'
    end
  end
end