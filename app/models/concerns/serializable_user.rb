class SerializableUser < JSONAPI::Serializable::Resource
  type 'users'

  attributes :email

  has_many :carriers
end