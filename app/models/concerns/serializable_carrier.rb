class SerializableCarrier < JSONAPI::Serializable::Resource
  type 'carriers'
  attributes :name

  belongs_to :user

  has_many :departure
end