class SerializableTicket < JSONAPI::Serializable::Resource
  type 'tickets'

  attributes :code

  belongs_to :user
  belongs_to :departure
  belongs_to :transaction
end