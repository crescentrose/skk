class Transaction < ApplicationRecord
  include ActiveModel::Validations

  validates_with CapacityValidator

  belongs_to :departure
  belongs_to :user, optional: true
  has_one :ticket

  before_validation do
    if attribute_present?('credit_card')
      # We never persist the full CC value.
      self.credit_card = credit_card
                         .gsub(/[^0-9]/, '')
                         .gsub!(/(\d{4})(\d*)(\d{4})/) { |match| "#{$1}#{'*' * $2.length}#{$3}" }
    end
  end

  before_save do
    # this should be amended eventually to allow for discounts, coupons,
    # penalties maybe... (like people with annoying children...)
    self.sum = departure.price.to_f
  end

  def self.create_with_ticket(create_params)
    this = create!(create_params)
    Ticket.create!(departure_id: this.departure_id, user: this.user, trans: this)
    this
  end
end
