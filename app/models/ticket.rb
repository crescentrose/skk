class Ticket < ApplicationRecord
  belongs_to :departure
  belongs_to :user, optional: true
  belongs_to :trans, class_name: 'Transaction', foreign_key: 'transaction_id'

  before_validation do
    # generate code
    self.code = SecureRandom.uuid.delete('-')
  end
end
