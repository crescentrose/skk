class Departure < ApplicationRecord
  belongs_to :carrier
  has_many :tickets

  validates_presence_of :source, :destination, :departure, :arrival, :capacity, :price

  validates :capacity, numericality: { greater_than_or_equal_to: 0 }

  def available_seats
    capacity - tickets.count
  end
end
