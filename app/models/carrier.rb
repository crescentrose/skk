class Carrier < ApplicationRecord
  belongs_to :user
  has_many :departures

  validates_presence_of :name
end