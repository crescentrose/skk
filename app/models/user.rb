class User < ApplicationRecord
  has_many :carriers, dependent: :destroy

  has_secure_password
  validates_presence_of :email

  def can_create_departures?
    is_administrator || carriers.count > 0
  end

  def can_edit_departure?(departure)
    is_administrator || departure.carrier.user_id == id
  end

  def owns_carrier?(carrier_id)
    carriers.select { |c| c.id == carrier_id.to_i }.count == 1
  end
end
