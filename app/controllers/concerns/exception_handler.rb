module ExceptionHandler
  extend ActiveSupport::Concern

  class InvalidToken < StandardError; end
  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class UnauthorizedAccess < StandardError; end

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      render jsonapi_errors: {detail: e.message}, status: :not_found
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      render jsonapi_errors: {detail: e.message}, status: :unprocessable_entity
    end

    rescue_from ExceptionHandler::InvalidToken do |e|
      render jsonapi_errors: {detail: e.message }, status: :forbidden
    end

    rescue_from ExceptionHandler::AuthenticationError do |e|
      render jsonapi_errors: { detail: e.message }, status: :unauthorized
    end

    rescue_from ExceptionHandler::MissingToken do
      render jsonapi_errors: { detail: 'Missing token.' }, status: :unauthorized
    end

    rescue_from ExceptionHandler::UnauthorizedAccess do
      render jsonapi_errors: { detail: 'Forbidden.' }, status: :forbidden
    end
  end

end