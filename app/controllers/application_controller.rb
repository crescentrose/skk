class ApplicationController < ActionController::API
  include ExceptionHandler

  before_action :authorize_request

  attr_reader :current_user

  protected

  def authorization_available?
    request.headers.include?('Authorization')
  end

  def authorize_request
    @current_user = AuthorizeApiRequest.new(request.headers).call[:user]
  end

end
