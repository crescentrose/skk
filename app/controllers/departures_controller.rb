class DeparturesController < ApplicationController

  skip_before_action :authorize_request, only: [:index, :show]

  deserializable_resource :departure, class: DeserializableDeparture, only: [:create, :update]

  # GET /departures
  def index
    render jsonapi: Departure.all, include: [:carrier]
  end

  # GET /departures/:id
  def show
    @departure = Departure.find(params[:id])
    render jsonapi: @departure, include: [:carrier]
  end

  # POST /departures
  def create
    raise ExceptionHandler::UnauthorizedAccess if !current_user.can_create_departures?
    raise ExceptionHandler::UnauthorizedAccess if !current_user.owns_carrier?(create_params[:carrier_id])

    @departure = Departure.create!(create_params)
    render jsonapi: @departure, include: [:carrier], status: :created
  end

  # PATCH /departures/:id
  def update
    @departure = Departure.find(params[:id])
    # TODO: find a better way than copy paste
    raise ExceptionHandler::UnauthorizedAccess if !current_user.can_edit_departure?(@departure)

    @departure.update!(create_params)
    render jsonapi: @departure, include: [:carrier], status: :ok
  end

  # DELETE /departures/:id
  def destroy
    @departure = Departure.find(params[:id])
    raise ExceptionHandler::UnauthorizedAccess if !current_user.can_edit_departure?(@departure)
    @departure.destroy!
    head :no_content
  end

  private

  def create_params
    params.require(:departure).permit(:source, :destination, :departure, :arrival, :capacity, :price, :carrier_id)
  end

end
