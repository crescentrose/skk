class TransactionsController < ApplicationController

  # We must let guests purchase transactions (sigh...)
  skip_before_action :authorize_request, only: [:create]

  deserializable_resource :transaction, class: DeserializableTransaction, only: [:create]

  # POST /transactions
  def create
    authorize_request if authorization_available?
    @transaction = Transaction.create_with_ticket(create_params)
    render jsonapi: @transaction, include: [:departure, :ticket], status: :created
  end

  # GET /transactions
  def index
    @transactions = Transaction.where(user: current_user)
    render jsonapi: @transactions, include: [:departure, :ticket]
  end

  # GET /transactions/:id
  def show
    @transactions = Transaction.find_by!(id: params[:id], user: current_user)
    render jsonapi: @transactions, include: [:departure, :ticket]
  end

  private

  def create_params
    add_user_to_params(params.require(:transaction).permit(:credit_card, :departure_id))
  end

  def add_user_to_params(params)
    if current_user
      params.merge(user: current_user)
    else
      params
    end
  end
end