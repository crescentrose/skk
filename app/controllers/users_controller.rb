class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create

  deserializable_resource :user, class: DeserializableUser, only: [:create]

  # POST /users
  def create
    @user = User.create!(user_params)
    render jsonapi: @user, status: :created
  end

  # GET /users/me
  def me
    render jsonapi: current_user
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end