class CarriersController < ApplicationController

  skip_before_action :authorize_request, only: [:index, :show]

  # GET /carriers
  def index
    render jsonapi: Carrier.all, include: [:user]
  end

  # GET /carriers/:id
  def show
    @carrier = Carrier.find(params[:id])

    render jsonapi: @carrier, include: [:user]
  end
end