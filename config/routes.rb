Rails.application.routes.draw do
  resources :carriers, only: [:index, :show]
  resources :departures
  resources :users, only: [:create]
  resources :transactions, only: [:index, :show, :create]

  get '/users/me', to: 'users#me'
  post 'auth/login', to: 'authentication#authenticate'
end
