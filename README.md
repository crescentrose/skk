# Sample ticketing system

Using ruby 2.5.1.

To set up, do the standard:
- `bundle install`
- `rails db:migrate`

The sample comes with a prepared user and carrier in `seeds.rb`.

Don't forget to run the tests! `bundle exec rspec` - I am slightly proud of most of them.

## API documentation

To browse the API documentation, point your browser to **[this Apiary document](https://crescentskk.docs.apiary.io).**
The documentation contains the description of every endpoint and what it does. I've also put some 
"developer commentary" (i.e. something I wouldn't usually put in the documentation) in italics. 

## Other notes

I haven't had the time to implement API versioning and some more specific endpoints that I wanted to (you can see the
original blueprint for the API in the Git history under /docs`, however I quickly realised I am not on my home turf and that I should
focus on the task, not having everything production ready), but other things from the task should work, as described in the
Apiary documentation. 

I also wanted to add a Docker image with the project and set up CI on the GitLab project so that it would look
impressive but I don't want to be even more late with the submission :)

I've enjoyed this little exercise a lot. I am sure that there are going to be pieces of code that will be slightly
upsetting to a seasoned Rails veteran, but I did my best (considering my last Rails experience was with Rails 3) and 
I've learned a hecking lot. Be gentle :)