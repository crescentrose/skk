require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  let!(:user) { create(:user) }
  let(:headers) { valid_headers(:resource) }
  let(:invalid_headers) {
    # TODO see why can I not use invalid_headers when I can use valid_headers
    {
      'Authorization' => nil,
      'Content-Type' => 'application/vnd.api+json'
    }
  }

  describe '#authorize_request' do
    context 'with auth token' do
      before { allow(request).to receive(:headers).and_return(headers) }

      it 'set current user' do
        expect(subject.instance_eval { authorize_request }).to eq(user)
      end
    end

    context 'without auth token' do
      before do
        allow(request).to receive(:headers).and_return(invalid_headers)
      end

      it 'raise token error' do
        expect { subject.instance_eval { authorize_request } }
          .to raise_error(ExceptionHandler::MissingToken, /Missing token/)
      end
    end
  end
end