require 'rails_helper'

RSpec.describe AuthorizeApiRequest do

  let(:user) { create(:user) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }
  subject(:invalid_request_obj) { described_class.new({}) }
  subject(:request_obj) { described_class.new(header) }

  describe '#call' do
    context 'when the request is valid' do
      it 'returns the user' do
        result = request_obj.call
        expect(result[:user]).to eq(user)
      end
    end

    context 'when the request is invalid' do
      subject(:invalid_request_obj) do
        described_class.new('Authorization' => token_generator(5))
      end

      it 'raises an invalid token error' do
        expect { invalid_request_obj.call }.to raise_error(ExceptionHandler::InvalidToken, /Invalid token/)
      end
    end

    context 'when the token has expired' do
      let(:header) { { 'Authorization' => expired_token_generator(user.id) } }
      subject(:invalid_request_obj) { described_class.new(header) }

      it 'raises the expired signature error' do
        expect { invalid_request_obj.call }.to raise_error(ExceptionHandler::InvalidToken, /Signature has expired/)
      end
    end

    context 'fake token' do
      let(:header) { { 'Authorization' => 'kifla' } }
      subject(:invalid_request_obj) { described_class.new(header) }

      it 'handles JWT::DecodeError' do
        expect { invalid_request_obj.call }.to raise_error(ExceptionHandler::InvalidToken)
      end
    end
  end

end