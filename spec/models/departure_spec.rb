require 'rails_helper'

RSpec.describe Departure, type: :model do
  it { should belong_to(:carrier) }
  %i[source destination departure arrival capacity price].each do |mandatory_field|
    it { should validate_presence_of(mandatory_field) }
  end
end
