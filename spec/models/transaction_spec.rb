require 'rails_helper'

RSpec.describe Transaction, type: :model do
  let (:transaction) { create(:transaction) }

  it { should belong_to(:user) }
  it { should belong_to(:departure) }

  it 'should obfuscate credit card numbers' do
    # e.g. 1234-5678-9012-3456 should be turned into 1234567890123456 and
    # then into 1234********3456
    expect(transaction.credit_card).to match(/\d{4}\*+\d{4}/)
  end

  it 'should have the same price as the provided departure' do
    expect(transaction.sum).to eq(transaction.departure.price)
  end
end
