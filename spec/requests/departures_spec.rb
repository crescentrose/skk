RSpec.describe 'Departures API', type: :request, api: true do
  let(:user) { create(:user_with_carrier) }
  let!(:departures) { create_list(:departure, 10, carrier: user.carriers.first) }
  let(:departure_id) { departures.first.id }
  let(:departure_arrival) { departures.first.arrival.strftime('%F %T') }
  let(:carrier_id) { departures.first.carrier.id }
  let(:headers) { valid_headers(:resource) }

  describe 'GET /departures' do
    before { get '/departures', params: {}, headers: headers}

    it 'returns departures' do
      expect(json).not_to be_empty
      expect(json['data'].size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /departures/:id' do
    before { get "/departures/#{departure_id}", params: {}, headers: headers }

    context 'when the departure exists' do
      it 'returns the departure' do
        expect(json).not_to be_empty
        expect(json['data']['id']).to eq(departure_id.to_s)
        expect(json['data']['attributes']['arrival']).to eq(departure_arrival)
      end

      it 'returns http ok' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the departure does not exist' do
      let(:departure_id) { 100 }

      it 'returns http 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found' do
        expect(response.body).to match(/Couldn't find Departure/)
      end
    end
  end


  describe 'POST /departures' do
    let(:valid_attributes) {
      {
        data: {
          type: 'departures',
          attributes: {
            source: 'Zagreb',
            destination: 'Rijeka',
            departure: '2018-05-16 14:00:00',
            arrival: '2018-05-16 16:20:00',
            capacity: 80,
            price: 45.95
          },
          relationships: {
            carrier: {
              data: {
                id: carrier_id.to_s,
                type: 'carriers'
              }
            }
          }
        }
      }.to_json
    }

    context 'when the request is valid' do

      before do
        post '/departures', params: valid_attributes, headers: headers
      end

      it 'creates a departure' do
        expect(json['data']['attributes']['source']).to eq('Zagreb')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before do
        params = {
          data: {
            type: 'departures',
            attributes: {
              source: 'kifla'
            },
            relationships: {
              carrier: {
                data: {
                  id: carrier_id.to_s,
                  type: 'carriers'
                }
              }
            }
          }
        }.to_json
        post '/departures', params: params, headers: headers
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/Validation failed/)
      end
    end

    context 'when the user has no carriers' do
      let(:user) { create(:user) }
      let(:departures) { create_list(:departure, 10) }

      before { post '/departures', params: valid_attributes, headers: headers }

      it 'returns http forbidden' do
        expect(response).to have_http_status(403)
      end
    end
  end


  describe 'PATCH /departures/:id' do
    before do
      data = {
        data: {
          id: departure_id.to_s,
          type: 'departures',
          attributes: {
            destination: 'Ravenholm'
          }
        }
      }.to_json
      patch "/departures/#{departure_id}", params: data, headers: headers
    end

    context 'when the departure exists' do
      it 'returns the updated departure' do
        expect(json).not_to be_empty
        expect(json['data']['id']).to eq(departure_id.to_s)
        expect(json['data']['attributes']['destination']).to eq('Ravenholm')
      end

      it 'returns http ok' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the departure does not exist' do
      let(:departure_id) { 100 }

      it 'returns http 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found' do
        expect(response.body).to match(/Couldn't find Departure/)
      end
    end

    context 'when the user is not the owner of the departure' do
      let(:user) { create(:user) }
      let(:departures) { create_list(:departure, 10) }

      it 'returns http forbidden' do
        expect(response).to have_http_status(403)
      end
    end
  end

  describe 'DELETE /departures/:id' do
    before do
      delete "/departures/#{departure_id}", params: {}, headers: headers
    end

    it 'returns no content' do
      expect(response).to have_http_status(204)
    end
  end
end