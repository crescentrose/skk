require 'rails_helper'

RSpec.describe 'Authentication', type: :request do
  describe 'POST /auth/login' do
    let!(:user) { create(:user) }
    let(:headers) { valid_headers(:authorization).except('Authorization') }
    let(:valid_credentials) do
      {
        email: user.email,
        password: user.password
      }.to_json
    end
    let(:invalid_credentials) do
      {
        email: 'pekarski-proizvodi@example.com',
        password: 'passw0rd'
      }.to_json
    end

    context 'when the request is valid' do
      before { post '/auth/login', params: valid_credentials, headers: headers }

      it 'returns an authorization token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when the request is invalid' do
      before { post '/auth/login', params: invalid_credentials, headers: headers }

      it 'returns an authorization token' do
        expect(json['errors'].first['detail']).to match(/Invalid credentials/)
      end
    end
  end
end