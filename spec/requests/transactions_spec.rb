RSpec.describe 'Transactions API', type: :request, api: true do
  let(:user) { create(:user) }
  let(:another_user) { create(:user) }
  let!(:transactions) { create_list(:transaction, 10, user: user) }
  let(:first_transaction) { transactions.first }
  let!(:foreign_transactions) { create_list(:transaction, 10, user: another_user) }
  let(:first_foreign) { foreign_transactions.first }

  describe 'GET /transactions' do
    context 'when the user is logged in' do
      before { get '/transactions', params: {}, headers: valid_headers }

      it 'should return a valid response' do
        expect(response).to have_http_status(200)
      end

      it 'should return the user\'s transactions' do
        json['data'].each do |transaction|
          matches = transactions.select { |t| t.id.to_s == transaction['id'] }
          expect(matches.count).to eq(1)
        end
      end

      it 'should not return the transactions that do not belong to the user' do
        json['data'].each do |transaction|
          matches = foreign_transactions.select { |t| t.id.to_s == transaction['id'] }
          expect(matches.count).to eq(0)
        end
      end
    end

    context 'when the user is not logged in' do
      before { get '/transactions' }

      it 'should return 401' do
        expect(response).to have_http_status(401)
      end
    end
  end


  describe 'GET /transactions/:id' do
    context 'when the user is logged in and accesses their transactions' do
      before { get "/transactions/#{first_transaction.id}", params: {}, headers: valid_headers }

      it 'should return http 200' do
        expect(response).to have_http_status(200)
      end

      it 'should let the user access their transaction' do
        expect(json['data']['id']).to eq(first_transaction.id.to_s)
      end

    end

    context 'when the user tries to access another users transactions' do
      before { get "/transactions/#{first_foreign.id}", params: {}, headers: valid_headers }

      it 'should return 404' do
        expect(response).to have_http_status(404)
      end
    end

    context 'when the user is not logged in' do
      before { get "/transactions/#{first_transaction.id}" }

      it 'should return 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'POST /transactions' do

    let(:example_transaction) do
      {
        data: {
          type: 'transactions',
          attributes: {
            creditCard: '377403365585026'
          },
          relationships: {
            departure: {
              data: {
                type: 'departures',
                id: transactions.first.departure.id.to_s
              }
            }
          }
        }
      }.to_json
    end

    context 'when the user is logged in' do
      before do
        post '/transactions', params: example_transaction, headers: valid_headers
      end

      it 'should respond with http 201' do
        expect(response).to have_http_status(201)
      end

      it 'should create a new transaction' do
        expect(json['data']['type']).to eq('transactions')
      end

      it 'should have a ticket' do
        expect(json['data']['relationships']['ticket']['data']['type']).to eq('tickets')
      end
    end

    context 'when the user is not logged in' do
      before do
        post '/transactions', params: example_transaction, headers: { 'Content-Type' => 'application/vnd.api+json' }
      end

      it 'should respond with http 201' do
        expect(response).to have_http_status(201)
      end

      it 'should create a new transaction' do
        expect(json['data']['type']).to eq('transactions')
      end

      it 'should have a ticket' do
        expect(json['data']['relationships']['ticket']['data']['type']).to eq('tickets')
      end
    end
  end
end