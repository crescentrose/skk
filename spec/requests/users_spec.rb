require 'rails_helper'

RSpec.describe 'Users API', type: :request, api: true do
  let(:user_request) do
    {
      data: {
        type: 'users',
        attributes: {
          email: 'ivan@hpup.co',
          password: Faker::Internet.password
        }
      }
    }.to_json
  end
  let(:user) { create(:user) }
  let(:headers) { { 'Content-Type' => 'application/vnd.api+json' } }

  describe 'POST /users' do
    before { post '/users', params: user_request, headers: headers }

    it 'saves the users details' do
      expect(json['data']['attributes']['email']).to eq('ivan@hpup.co')
    end

    it 'returns http created' do
      expect(response).to have_http_status(201)
    end
  end

  describe 'GET /users/me' do
    context 'when the user is logged in' do
      before { get '/users/me', params: {}, headers: valid_headers }

      it 'returns the currently logged in user' do
        expect(json['data']['attributes']['email']).to eq(user.email)
      end
    end

    context 'when the user is not logged in' do
      before { get '/users/me', params: {}, headers: headers }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end