require 'rails_helper'

RSpec.describe 'Carriers API', type: :request do
  let!(:carriers) { create_list(:carrier, 10) }
  let(:carrier_id) { carriers.first.id }

  describe 'GET /carriers' do
    before { get '/carriers' }

    it 'returns carriers' do
      expect(json).not_to be_empty
      expect(json['data'].size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /carriers/:id' do
    before { get "/carriers/#{carrier_id}" }

    context 'when the carrier exists' do
      it 'returns the carrier' do
        expect(json).not_to be_empty
        # IDs in JSONAPI are always strings
        expect(json['data']['id']).to eq(carrier_id.to_s)
      end

      it 'returns http ok' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the carrier does not exist' do
      let(:carrier_id) { 100 }

      it 'returns http 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found' do
        expect(response.body).to match(/Couldn't find Carrier/)
      end
    end
  end
end