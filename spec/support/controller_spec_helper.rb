module ControllerSpecHelper
  def token_generator(user_id)
    JsonWebToken.encode(user_id: user_id)
  end

  def expired_token_generator(user_id)
    JsonWebToken.encode({ user_id: user_id }, 10.minutes.ago)
  end

  def valid_headers(server = :resource)
    {
      resource: {
        'Authorization' => token_generator(user.id),
        'Content-Type' => 'application/vnd.api+json'
      }, authorization: {
        'Authorization' => token_generator(user.id),
        'Content-Type' => 'application/json'
      }
    }[server]
  end

  def invalid_headers(server = :resource)
    {
      resource: {
        'Authorization' => nil,
        'Content-Type' => 'application/vnd.api+json'
      }, authorization: {
        'Authorization' => nil,
        'Content-Type' => 'application/json'
      }
    }[server]
  end
end