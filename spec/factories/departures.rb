FactoryBot.define do
  factory :departure do
    source { Faker::Overwatch.location }
    destination { Faker::Overwatch.location }
    departure_time = Faker::Time.forward()
    departure { departure_time }
    arrival { departure_time + 2.hours }
    capacity { Faker::Number.between(10, 50) }
    price { Faker::Number.decimal(2, 2) }

    carrier
  end
end