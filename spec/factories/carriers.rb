FactoryBot.define do
  factory :carrier do
    name {
      Faker::Overwatch.hero + [
        ' Tours', '-trans', '  Transport Company', 'Bus', ' Ticketing System'
      ].sample
    }
    user
  end

  factory :carrier_with_departure do
    transient { departure_count 10 }

    after(:create) do |carrier, evaluator|
      create_list(:departure, evaluator.departure_count, carrier: carrier)
    end
  end
end