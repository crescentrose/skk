FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password }

    factory :user_with_carrier do
      transient { carrier_count 1 }

      after(:create) do |user, evaluator|
        create_list(:carrier, evaluator.carrier_count, user: user)
      end
    end

    factory :user_with_transaction do
      transient { transaction_count 10 }

      after(:create) do |user, evaluator|
        create_list(:transaction, evaluator.transaction_count, user: user)
      end
    end
  end
end