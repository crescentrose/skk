FactoryBot.define do
  factory :transaction do
    credit_card { Faker::Business.credit_card_number }
    sum { Faker::Number.decimal(2, 2) }

    departure
    user
  end
end